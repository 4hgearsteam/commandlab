#include "Robot.h"
#include <Subsystems/DriveSystem.h>
#include "CommandBase.h"
#include "Commands/Scheduler.h"

DriveSystem *CommandBase::driveSystem = NULL;
OI *CommandBase::oi = NULL;

// Initialize a single static instance of all of your subsystems to NULL
CommandBase::CommandBase(const std::string &name) :
		Command(name)
{
}

CommandBase::CommandBase() :
		Command()
{

}

void CommandBase::init()
{
	// Create a single static instance of all of your subsystems. The following
	// line should be repeated for each subsystem in the project.
	oi = new OI();
	driveSystem = new DriveSystem();
}
