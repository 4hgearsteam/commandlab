#ifndef DriveCommand_H
#define DriveCommand_H

#include "../CommandBase.h"
#include "WPILib.h"


const float DRIVE_CMD_TIMEOUT = 0.0;  // No timeout

class DriveCmd: public CommandBase
{
public:
	DriveCmd();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	float CommandTimeout;
};

#endif
