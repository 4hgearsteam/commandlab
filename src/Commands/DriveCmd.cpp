#include <Commands/DriveCmd.h>
#include "../OI.h"
#include "../Robot.h"
#include "../RobotMap.h"


DriveCmd::DriveCmd(): CommandBase("DriveCmd"), CommandTimeout(DRIVE_CMD_TIMEOUT)
{
	Requires(driveSystem);
	SetTimeout(CommandTimeout);
}

// Called just before this Command runs the first time
void DriveCmd::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void DriveCmd::Execute()
{
	Joystick& joystick = CommandBase::oi->get_joystick();

	driveSystem->Drive(joystick.GetRawAxis(LEFT_STICK_X_AXIS),
			joystick.GetRawAxis(LEFT_STICK_Y_AXIS),
			joystick.GetRawAxis(RIGHT_STICK_X_AXIS));
}

// Make this return true when this Command no longer needs to run execute()
bool DriveCmd::IsFinished()
{
	return false;
}

// Called once after isFinished returns true
void DriveCmd::End()
{
	driveSystem->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void DriveCmd::Interrupted()
{
	driveSystem->Stop();
}
