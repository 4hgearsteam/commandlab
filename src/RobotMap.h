#ifndef ROBOTMAP_H
#define ROBOTMAP_H

#include "WPILib.h"

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
 
// For example to map the left and right motors, you could define the
// following variables to use with your drivetrain subsystem.
//const int LEFTMOTOR = 1;
//const int RIGHTMOTOR = 2;

// If you are using multiple modules, make sure to define both the port
// number and the module. For example you with a range finder:
//const int RANGE_FINDER_PORT = 1;
//const int RANGE_FINDER_MODULE = 1;


const int BUTTON_BLUE                       = 3;
const int BUTTON_GREEN                      = 1;
const int BUTTON_RED                        = 2;
const int BUTTON_YELLOW                     = 4;

const int BUMPER_BUTTON_LEFT                = 5;
const int BUMPER_BUTTON_RIGHT               = 6;
const int TRIGGER_BUTTON_LEFT               = 2;
const int TRIGGER_BUTTON_RIGHT              = 3;

const uint32_t LEFT_STICK_X_AXIS            = 0;
const uint32_t LEFT_STICK_Y_AXIS            = 1;
const uint32_t RIGHT_STICK_X_AXIS           = 4;


// Joystick function mapping
const int CLAW_OPEN_BUTTON                  = BUTTON_GREEN;
const int CLAW_CLOSE_BUTTON                 = BUTTON_BLUE;
const int ARM_OUT_BUTTON                    = BUTTON_YELLOW;
const int ARM_IN_BUTTON                     = BUTTON_RED;



// Mecanum Drive Motor Channel Declarations
const uint32_t MOTOR_FRONT_LEFT_CHANNEL     = 2;
const uint32_t MOTOR_REAR_LEFT_CHANNEL      = 3;
const uint32_t MOTOR_FRONT_RIGHT_CHANNEL    = 1;
const uint32_t MOTOR_REAR_RIGHT_CHANNEL     = 4;


// Claw Solenoid Channels
const uint32_t CLAW_PCM                     = 1;
const uint32_t CLAW_OPEN_CHANNEL            = 0;
const uint32_t CLAW_CLOSE_CHANNEL           = 1;

// Arm Solenoid Channels
const uint32_t ARM_PCM                      = 1;
const uint32_t ARM_OUT_CHANNEL              = 2;
const uint32_t ARM_IN_CHANNEL               = 3;

/*
 * Dashboard control
 */
#define SDASH_STRING_LEFT_FIRST    "DB/String 0"
#define SDASH_STRING_LEFT_SECOND   "DB/String 1"
#define SDASH_STRING_LEFT_THIRD    "DB/String 2"
#define SDASH_STRING_LEFT_FOURTH   "DB/String 3"
#define SDASH_STRING_LEFT_FIFTH    "DB/String 4"

#define SDASH_STRING_RIGHT_FIRST   "DB/String 5"
#define SDASH_STRING_RIGHT_SECOND  "DB/String 6"
#define SDASH_STRING_RIGHT_THIRD   "DB/String 7"
#define SDASH_STRING_RIGHT_FOURTH  "DB/String 8"
#define SDASH_STRING_RIGHT_FIFTH   "DB/String 9"

#endif
