/*
 * Robot.h
 *
 *  Created on: Aug 14, 2016
 *      Author: Student
 */

#ifndef SRC_ROBOT_H_
#define SRC_ROBOT_H_

#include "WPILib.h"
#include "Commands/Command.h"

#include "OI.h"

class Robot: public IterativeRobot {
public:
	static LiveWindow *lw;


private:
	std::unique_ptr<Command> autonomousCommand;
	SendableChooser *chooser;

	void RobotInit();
	void DisabledInit();
	void DisabledPeriodic();
	void AutonomousInit();
	void AutonomousPeriodic();
	void TeleopInit();
	void TeleopPeriodic();
	void TestPeriodic();
};


#endif /* SRC_ROBOT_H_ */
