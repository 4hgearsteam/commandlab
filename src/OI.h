#ifndef OI_H
#define OI_H

#include "WPILib.h"

class OI
{
private:
	Joystick m_joy;


public:
	OI();
	Joystick& get_joystick();
};

#endif
